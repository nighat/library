<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/21/2018
 * Time: 12:54 AM
 */

namespace Pondit\Library;


class Book
{
    public $book_name;


    public function __construct($book_name)
    {
        $this->book_name = $book_name;
    }


    public function book()
    {
        $result = "This is Book Name--".$this->book_name;
        return $result;

    }
}